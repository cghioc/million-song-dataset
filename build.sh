#!/bin/bash

export HADOOP_HOME=/home/claudiu/masd/3tema/hadoop-2.5.1

rm -rf output*

mkdir hadoop_out


# Compile all the jobs
javac -classpath \
	$HADOOP_HOME/share/hadoop/common/hadoop-common-2.5.1.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.5.1.jar:$HADOOP_HOME/share/hadoop/common/lib/commons-cli-1.2.jar \
	-d hadoop_out engine/*.java util/*.java
jar -cvf main.jar -C hadoop_out .


## Run the first job
#echo "####### Run the first job #########"
#$HADOOP_HOME/bin/hadoop jar main.jar engine.LoudnessTempo \
#	MillionSongSubsetConverted output1
#cat output1/part-00000 | tail -n +2 | sed 's/\t//g' > output1/task1_out.dat
#./scripts/plot_loudness.sh
#./scripts/plot_tempo.sh
#
#
## Run the second job
#echo
#echo "####### Run the second job #########"
#$HADOOP_HOME/bin/hadoop jar main.jar engine.GenreJob\
#	MillionSongSubsetConverted output2
#
#
## Run the third job
#echo
#echo "####### Run the third job #########"
#$HADOOP_HOME/bin/hadoop jar main.jar engine.ModeJob\
#	MillionSongSubsetConverted output3
#cat output3/part-00000 | grep -v NaN | tail -n +2 | sed 's/\t//g' > output3/task1_out.dat
#./scripts/plot_mode_loudness.sh
#./scripts/plot_mode_tempo.sh



# Run the fourth job
echo
echo "####### Run the fourth job #########"
$HADOOP_HOME/bin/hadoop jar main.jar engine.CrossoverJob\
	MillionSongSubsetConverted output4
