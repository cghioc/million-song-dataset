package engine;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

import util.Genre;
import util.GenreDetector;
import util.InputParser;
import util.SongProperty;

public class CrossoverJob {
	public static Set<SongProperty> props = new HashSet<SongProperty>();
	public Genre currentGenre;
	
	public static class GenreKey implements WritableComparable<GenreKey> {
		public Genre genre;

		public GenreKey(){}

		public GenreKey(Genre genre) {
			this.genre = genre;
		}

		/* serializes the key*/
		public void write(DataOutput out) throws IOException
		{
			/* write all relevant members in an arbitrary order */
			out.writeInt(genre.ordinal());
		}

		/* deserialize the key */
		public void readFields(DataInput in) throws IOException
		{
			/* read all relevant members in the same order they were written in */
			genre = Genre.values()[in.readInt()];
		}

		public int compareTo(GenreKey t)
		{
			if (t.genre.ordinal() < this.genre.ordinal())
				return 1;
			if (t.genre.ordinal() > this.genre.ordinal())
				return -1;
			return 0;
		}

		public String toString()
		{
			return "" + genre;
		}
	}

	public static class Presence implements Writable {
		public int[] presence = new int[Genre.values().length];

		public void write(DataOutput out) throws IOException {
			for (int i = 0; i < Genre.values().length; i++)
				out.writeInt(presence[i]);
		}

		public void readFields(DataInput in) throws IOException {
			for (int i = 0; i < Genre.values().length; i++)
				presence[i] = in.readInt();
		}

		public void addPresence(Presence pres) {
			for (int i = 0; i < Genre.values().length; i++)
				presence[i] += pres.presence[i];
		}
		
		public void toPercentage(int genreKeyOrdinal) {
			int genreKeyPresence = presence[genreKeyOrdinal];

			for (int i = 0; i < Genre.values().length; i++) {
				if (i == genreKeyOrdinal) {
					presence[i] = 100;
					continue;
				}
				presence[i] = 100 * presence[i] / genreKeyPresence;
			}
		}


		public String toString() {
			String s = "";
			for (int i = 0; i < Genre.values().length; i++)
				s += presence[i] + " ";
			return s;
		}
	}

	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, GenreKey, Presence> {
		private Presence pres = new Presence();
		private float [] atf;
		private String [] artistTerms, artistTermsFreqs;
		private GenreDetector gd = new GenreDetector();
		private int count = 0;

		public void map(LongWritable key, Text value, 
				OutputCollector<GenreKey, Presence> output, Reporter reporter)
				throws IOException {
			int [] presence = new int[Genre.values().length];
			for (int i = 0; i < Genre.values().length; i++)
				presence[i] = 0;

			HashMap<SongProperty, String> songProps = InputParser.getSongProperties(value.toString(), props);
			
			
			/* Determine the presence of each genre in the song */
			artistTerms = songProps.get(SongProperty.ARTIST_TERMS).split(",");
			artistTermsFreqs = songProps.get(SongProperty.ARTIST_TERMS_FREQ).split(",");
			atf = new float[artistTermsFreqs.length];
			count = 0;
			for (String freq : artistTermsFreqs)
				if (freq.isEmpty())
					atf[count++] = 0;
				else
					atf[count++] = Float.parseFloat(freq);
			gd.computePresence(presence, artistTerms, atf);
			pres.presence = presence;
			//System.out.println("Got this presence: " + pres);
			
			
			/* For each existing genre in the song, collect the total presence */
			for (int i = 0; i < Genre.values().length; i++) {
				if (presence[i] == 0)
					continue;
				output.collect(new GenreKey(Genre.values()[i]), pres);
			}
		}
	}

	public static class Reduce extends MapReduceBase implements Reducer<GenreKey, Presence, GenreKey, Presence> {
		public void reduce(GenreKey key, Iterator<Presence> values, 
				OutputCollector<GenreKey, Presence> output, Reporter reporter) throws IOException {
			Presence finalPresence = new Presence();

			while (values.hasNext()) {
				Presence input = values.next();
				finalPresence.addPresence(input);
			}
			System.out.println("For Genre " + key.genre + " presence is " + finalPresence);
			finalPresence.toPercentage(key.genre.ordinal());

			output.collect(key, finalPresence);
		}
	}

	public static void main(String []args) {

		/* Check the number of arguments */
		if (args.length < 2) {
			System.out.println("Provide the path to the song " +
					"directory and the output directory");
			System.exit(1);
		}
		props.add(SongProperty.ARTIST_TERMS);
		props.add(SongProperty.ARTIST_TERMS_FREQ);

		JobConf conf = new JobConf(LoudnessTempo.class);
		conf.setJobName("crossover");

		conf.setOutputKeyClass(GenreKey.class);
		conf.setOutputValueClass(Presence.class);

		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		/* Add all the files to the input file formatter */
		File dir = new File(args[0]);
		for (File file : dir.listFiles())
			FileInputFormat.addInputPath(conf, new Path(file.getAbsolutePath()));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		try {
			JobClient.runJob(conf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}