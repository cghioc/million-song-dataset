package engine;

import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

import util.Genre;
import util.GenreDetector;
import util.InputParser;
import util.SongProperty;

public class GenreJob {
	public static Set<SongProperty> props = new HashSet<SongProperty>();

	public static class Result {
		public String genre;
		public float loudness, tempo;
		public int count;

		public Result(String genre, float loudness, float tempo, int count) {
			this.genre = genre;
			this.loudness = loudness;
			this.tempo = tempo;
			this.count = count;
		}

		public String toString() {
			return "" + genre + "," + loudness + "," + tempo + "," + count;
		}
	}


	public static class GenreKey implements WritableComparable<GenreKey> {
		public Genre genre;

		public GenreKey(){}

		public GenreKey(Genre genre) {
			this.genre = genre;
		}

		/* serializes the key*/
		public void write(DataOutput out) throws IOException
		{
			/* write all relevant members in an arbitrary order */
			out.writeInt(genre.ordinal());
		}

		/* deserialize the key */
		public void readFields(DataInput in) throws IOException
		{
			/* read all relevant members in the same order they were written in */
			genre = Genre.values()[in.readInt()];
		}

		public int compareTo(GenreKey t)
		{
			if (t.genre.ordinal() < this.genre.ordinal())
				return 1;
			if (t.genre.ordinal() > this.genre.ordinal())
				return -1;
			return 0;
		}

		public String toString()
		{
			return "" + genre;
		}
	}


	public static class LoudTempo implements Writable {
		float loudness;
		float tempo;
		int count;

		public void write(DataOutput out) throws IOException {
			out.writeFloat(loudness);
			out.writeFloat(tempo);
			out.writeInt(count);
		}

		public void readFields(DataInput in) throws IOException {
			loudness = in.readFloat();
			tempo = in.readFloat();
			count = in.readInt();
		}

		public String toString() {
			return " " + loudness + " " + tempo + " " + count; 
		}
	}


	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, GenreKey, LoudTempo> {
		private LoudTempo lt = new LoudTempo();
		private String [] artistTerms, artistTermsFreqs;
		private float []atf;
		private int count = 0;
		private GenreDetector gd = new GenreDetector();
		private Genre genre;
		private GenreKey gk;

		public void map(LongWritable key, Text value, OutputCollector<GenreKey, LoudTempo> output, Reporter reporter)
				throws IOException {
			HashMap<SongProperty, String> songProps = InputParser.getSongProperties(value.toString(), props);

			lt.loudness	= Float.parseFloat(songProps.get(SongProperty.LOUDNESS));
			lt.tempo	= Float.parseFloat(songProps.get(SongProperty.TEMPO));
			lt.count	= 1;

			artistTerms = songProps.get(SongProperty.ARTIST_TERMS).split(",");
			artistTermsFreqs = songProps.get(SongProperty.ARTIST_TERMS_FREQ).split(",");
			atf = new float[artistTermsFreqs.length];
			count = 0;
			for (String freq : artistTermsFreqs)
				if (freq.isEmpty())
					atf[count++] = 0;
				else
					atf[count++] = Float.parseFloat(freq);
			genre = gd.detectGenre(artistTerms, atf);

			/* Build key and value */
			gk = new GenreKey(genre);

			output.collect(gk, lt);
		}
	}


	public static class Reduce extends MapReduceBase implements Reducer<GenreKey, LoudTempo, GenreKey, LoudTempo> {
		public void reduce(GenreKey key, Iterator<LoudTempo> values, 
				OutputCollector<GenreKey, LoudTempo> output, Reporter reporter) throws IOException {
			float loudnessSum = 0, tempoSum = 0;
			LoudTempo lt = new LoudTempo();
			int count = 0;

			while (values.hasNext()) {
				LoudTempo input = values.next();

				loudnessSum += input.loudness;
				tempoSum += input.tempo;
				count++;
			}

			lt.loudness	= loudnessSum / count;
			lt.tempo	= tempoSum / count;
			lt.count	= count;

			output.collect(key, lt);
		}
	}


	public static void sortResults(String outputFile) {
		/* Read output file */
		File output = new File(outputFile + "/part-00000");
		String line;
		String []words;

		Comparator<Result> countComparator = new Comparator<Result>() {
			public int compare(Result r1, Result r2) {
				if (r1.count > r2.count)
					return 1;
				if (r1.count < r2.count)
					return -1;
				return 0;
			}
		};
		Comparator<Result> loudnessComparator = new Comparator<Result>() {
			public int compare(Result r1, Result r2) {
				if (r1.loudness > r2.loudness)
					return 1;
				if (r1.loudness < r2.loudness)
					return -1;
				return 0;
			}
		};
		Comparator<Result> tempoComparator = new Comparator<Result>() {
			public int compare(Result r1, Result r2) {
				if (r1.tempo > r2.tempo)
					return 1;
				if (r1.tempo < r2.tempo)
					return -1;
				return 0;
			}
		};

		ArrayList<Result> countQueue = new ArrayList<Result>();
		ArrayList<Result> loudnessQueue = new ArrayList<Result>();
		ArrayList<Result> tempoQueue = new ArrayList<Result>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(output));
			while ((line = br.readLine()) != null) {
				System.out.println("Line " + line);
				words = line.split("[ |\t]");
				Result result = new Result(words[0], Float.parseFloat(words[2]),
						Float.parseFloat(words[3]), Integer.parseInt(words[4]));

				countQueue.add(result);
				loudnessQueue.add(result);
				tempoQueue.add(result);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Collections.sort(countQueue, countComparator);
		Collections.sort(loudnessQueue, loudnessComparator);
		Collections.sort(tempoQueue, tempoComparator);

		System.out.println();
		System.out.println("Genre sorted by number of songs");
		for (Result result : countQueue)
			System.out.println("Genre " + result.genre + " songs " + result.count);

		System.out.println();
		System.out.println("Genre sorted by loudness");
		for (Result result : loudnessQueue)
			System.out.println("Genre " + result.genre + " loudness " + result.loudness);

		System.out.println();
		System.out.println("Genre sorted by tempo");
		for (Result result : tempoQueue)
			System.out.println("Genre " + result.genre + " tempo " + result.tempo);
	}

	public static void main(String []args) {

		/* Check the number of arguments */
		if (args.length < 2) {
			System.out.println("Provide the path to the song " +
					"directory and the output directory");
			System.exit(1);
		}
		props.add(SongProperty.LOUDNESS);
		props.add(SongProperty.TEMPO);
		props.add(SongProperty.ARTIST_TERMS);
		props.add(SongProperty.ARTIST_TERMS_FREQ);

		JobConf conf = new JobConf(LoudnessTempo.class);
		conf.setJobName("genre");

		conf.setOutputKeyClass(GenreKey.class);
		conf.setOutputValueClass(LoudTempo.class);

		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		/* Add all the files to the input file formatter */
		File dir = new File(args[0]);
		for (File file : dir.listFiles())
			FileInputFormat.addInputPath(conf, new Path(file.getAbsolutePath()));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		try {
			JobClient.runJob(conf);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the output file and sort the results */
		sortResults(args[1]);
	}
}