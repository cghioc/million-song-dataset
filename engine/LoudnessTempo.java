package engine;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

import util.InputParser;
import util.SongProperty;

public class LoudnessTempo {
	public static Set<SongProperty> props = new HashSet<SongProperty>();

	public static class LoudTempo implements Writable {
		float loudness;
		float tempo;

		public void write(DataOutput out) throws IOException {
			out.writeFloat(loudness);
			out.writeFloat(tempo);
		}

		public void readFields(DataInput in) throws IOException {
			loudness = in.readFloat();
			tempo = in.readFloat();
		}

		public String toString() {
			return " " + loudness + " " + tempo + " "; 
		}
	}

	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, LongWritable, LoudTempo> {
		private LoudTempo lt = new LoudTempo();
		private int year;

		public void map(LongWritable key, Text value, 
				OutputCollector<LongWritable, LoudTempo> output, Reporter reporter)
				throws IOException {
			//System.out.print("Mapper for " + value.toString().substring(0, 10));
			HashMap<SongProperty, String> songProps = InputParser.getSongProperties(value.toString(), props);

			lt.loudness	= Float.parseFloat(songProps.get(SongProperty.LOUDNESS));
			lt.tempo	= Float.parseFloat(songProps.get(SongProperty.TEMPO));
			year		= Integer.parseInt(songProps.get(SongProperty.YEAR));
			//System.out.println(" Year " + year + " tempo " + lt.tempo + " loudness " + lt.loudness);

			output.collect(new LongWritable(year), lt);
		}
	}

	public static class Reduce extends MapReduceBase implements Reducer<LongWritable, LoudTempo, LongWritable, LoudTempo> {
		public void reduce(LongWritable key, Iterator<LoudTempo> values, 
				OutputCollector<LongWritable, LoudTempo> output, Reporter reporter) throws IOException {
			float loudnessSum = 0, tempoSum = 0;
			LoudTempo lt = new LoudTempo();
			int count = 0;

			while (values.hasNext()) {
				LoudTempo input = values.next();
				loudnessSum += input.loudness;
				tempoSum += input.tempo;
				count++;
			}

			lt.loudness	= loudnessSum / count;
			lt.tempo	= tempoSum / count;

			output.collect(key, lt);
		}
	}

	public static void main(String []args) {

		/* Check the number of arguments */
		if (args.length < 2) {
			System.out.println("Provide the path to the song " +
					"directory and the output directory");
			System.exit(1);
		}
		props.add(SongProperty.LOUDNESS);
		props.add(SongProperty.TEMPO);
		props.add(SongProperty.YEAR);

		JobConf conf = new JobConf(LoudnessTempo.class);
		conf.setJobName("loudness");

		conf.setOutputKeyClass(LongWritable.class);
		conf.setOutputValueClass(LoudTempo.class);

		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		/* Add all the files to the input file formatter */
		File dir = new File(args[0]);
		for (File file : dir.listFiles())
			FileInputFormat.addInputPath(conf, new Path(file.getAbsolutePath()));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		try {
			JobClient.runJob(conf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}