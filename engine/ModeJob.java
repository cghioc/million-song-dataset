package engine;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

import util.InputParser;
import util.SongProperty;

public class ModeJob {
	public static Set<SongProperty> props = new HashSet<SongProperty>();

	public static class LoudTempo implements Writable {
		float minorLoudness, majorLoudness;
		float minorTempo, majorTempo;
		int minorCount, majorCount;

		public void write(DataOutput out) throws IOException {
			out.writeFloat(minorLoudness);
			out.writeFloat(minorTempo);
			out.writeFloat(majorLoudness);
			out.writeFloat(majorTempo);
			out.writeInt(minorCount);
			out.writeInt(majorCount);
		}

		public void readFields(DataInput in) throws IOException {
			minorLoudness = in.readFloat();
			minorTempo = in.readFloat();
			majorLoudness = in.readFloat();
			majorTempo = in.readFloat();
			minorCount = in.readInt();
			majorCount = in.readInt();
		}

		public String toString() {
			return " " + minorLoudness + " " + minorTempo + " " +
					majorLoudness + " " + majorTempo + " " +
					minorCount + " " + majorCount; 
		}
	}

	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, LongWritable, LoudTempo> {
		private LoudTempo lt = new LoudTempo();
		private int year, mode;

		public void map(LongWritable key, Text value, 
				OutputCollector<LongWritable, LoudTempo> output, Reporter reporter)
				throws IOException {
			HashMap<SongProperty, String> songProps = InputParser.getSongProperties(value.toString(), props);

			mode		= Integer.parseInt(songProps.get(SongProperty.MODE));
			if (mode == 1) {
				lt.majorLoudness = Float.parseFloat(songProps.get(SongProperty.LOUDNESS));
				lt.majorTempo = Float.parseFloat(songProps.get(SongProperty.TEMPO));
				lt.majorCount = 1;
				lt.minorCount = 0;
			} else {
				lt.minorLoudness = Float.parseFloat(songProps.get(SongProperty.LOUDNESS));
				lt.minorTempo = Float.parseFloat(songProps.get(SongProperty.TEMPO));
				lt.minorCount = 1;
				lt.majorCount = 0;
			}
			year		= Integer.parseInt(songProps.get(SongProperty.YEAR));

			output.collect(new LongWritable(year), lt);
		}
	}

	public static class Reduce extends MapReduceBase implements Reducer<LongWritable, LoudTempo, LongWritable, LoudTempo> {
		public void reduce(LongWritable key, Iterator<LoudTempo> values, 
				OutputCollector<LongWritable, LoudTempo> output, Reporter reporter) throws IOException {
			float minorLoudnessSum = 0, minorTempoSum = 0,
					majorLoudnessSum = 0, majorTempoSum = 0;
			LoudTempo lt = new LoudTempo();
			int minorCount = 0, majorCount = 0;

			while (values.hasNext()) {
				LoudTempo input = values.next();
				
				if (input.majorCount == 1) {
					majorLoudnessSum += input.majorLoudness;
					majorTempoSum += input.majorTempo;
					majorCount++;
				} else {
					minorLoudnessSum += input.minorLoudness;
					minorTempoSum += input.minorTempo;
					minorCount++;
				}
			}

			lt.minorLoudness 	= minorLoudnessSum / minorCount;
			lt.minorTempo		= minorTempoSum / minorCount;
			lt.majorLoudness	= majorLoudnessSum / majorCount;
			lt.majorTempo		= majorTempoSum / majorCount;
			lt.minorCount		= minorCount;
			lt.majorCount		= majorCount;

			output.collect(key, lt);
		}
	}

	public static void main(String []args) {

		/* Check the number of arguments */
		if (args.length < 2) {
			System.out.println("Provide the path to the song " +
					"directory and the output directory");
			System.exit(1);
		}
		props.add(SongProperty.LOUDNESS);
		props.add(SongProperty.TEMPO);
		props.add(SongProperty.YEAR);
		props.add(SongProperty.MODE);

		JobConf conf = new JobConf(LoudnessTempo.class);
		conf.setJobName("mode");

		conf.setOutputKeyClass(LongWritable.class);
		conf.setOutputValueClass(LoudTempo.class);

		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		/* Add all the files to the input file formatter */
		File dir = new File(args[0]);
		for (File file : dir.listFiles())
			FileInputFormat.addInputPath(conf, new Path(file.getAbsolutePath()));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		try {
			JobClient.runJob(conf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}