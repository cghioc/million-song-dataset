#!/usr/bin/gnuplot 
reset
set title "Song Loudness"
set terminal png
set xlabel "Year"
set ylabel "Loudness"


set key reverse Left outside
set grid
set style data linespoints


set output 'png/loudness.png'
plot "output1/task1_out.dat" using 1:2 title "Loudness"
