#!/usr/bin/gnuplot 
reset
set title "Song Loudness Based on Mode"
set terminal png
set xlabel "Year"
set ylabel "Loudness"


set key reverse Left outside
set grid
set style data linespoints


set output 'png/mode_loudness.png'
plot "output3/task1_out.dat" using 1:2 title "Minor Loudness", \
	"output3/task1_out.dat" using 1:4 title "Major Loudness"
