#!/usr/bin/gnuplot 
reset
set title "Song Tempo Based on Mode"
set terminal png
set xlabel "Year"
set ylabel "Tempo"


set key reverse Left outside
set grid
set style data linespoints


set output 'png/mode_tempo.png'
plot "output3/task1_out.dat" using 1:3 title "Minor Tempo", \
	"output3/task1_out.dat" using 1:5 title "Major Tempo"
