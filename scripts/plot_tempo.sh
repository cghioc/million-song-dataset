#!/usr/bin/gnuplot 
reset
set title "Song Tempo"
set terminal png
set xlabel "Year"
set ylabel "Tempo"


set key reverse Left outside
set grid
set style data linespoints


set output 'png/tempo.png'
plot "output1/task1_out.dat" using 1:3 title "Tempo"
